import { NgModule } from '@angular/core';
import {MatCardModule} from '@angular/material/card';

const materialModule = [
  MatCardModule
];



@NgModule({
  exports:[materialModule]
})
export class MaterialModule { }
