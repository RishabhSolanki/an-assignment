import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpclientService } from '../service/httpclient.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  loadFlag = true;
  constructor(private route:ActivatedRoute, private _productService: HttpclientService) { }
  productId:string;
  product:any;
  flag = true;
  ngOnInit(): void {
    this.productId = this.route.snapshot.params['id']
    this._productService.getProduct(this.productId).subscribe(res=>{
      this.product = res;
      if(res){
        this.loadFlag = false;
      }
      console.log(this.product.name)
      if (this.product.length === 0){
        this.flag = false;
      }
      console.log(this.product)
    })
  }

}
