import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class HttpclientService {

  constructor(private http: HttpClient) { }
  listItems(){
    return this.http.get<any>('https://api.growcify.com/dev/category/list');
  }

  getProduct(id){
    return this.http.get(`https://api.growcify.com/dev/product/list/${id}`)
  }
}
