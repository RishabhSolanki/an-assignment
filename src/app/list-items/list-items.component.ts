import { Component, OnInit } from '@angular/core';
import { HttpclientService } from '../service/httpclient.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-list-items',
  templateUrl: './list-items.component.html',
  styleUrls: ['./list-items.component.css']
})
export class ListItemsComponent implements OnInit {

  listItem:any;
  loadFlag = false;
  constructor(private _listSerice: HttpclientService, private router: Router,private SpinnerService: NgxSpinnerService) { }

  ngOnInit(): void {
    this.listItem = this.listData();
  }

  showProduct(item){
    let id = item._id;
    this.router.navigate(['product',id])
  }
  listData(){
    this._listSerice.listItems().subscribe(res=>{
      this.listItem = res;
      if(res){
        this.loadFlag = false;
      }
    })
    return this._listSerice.listItems();
  }

}
